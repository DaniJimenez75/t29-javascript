

mostrarResultados(randomSumaDados(36000));

function randomSumaDados(numVeces) {
    var resultados = new Array(11);
    for (var j = 0; j < resultados.length; j++) {
        resultados[j] = 0
    }
    var res;
    for (var i = 0; i < numVeces; i++) {
        res = aleatorioEntero(1, 6) + aleatorioEntero(1, 6);
        resultados[res - 2]++;
    }
    return resultados;

}

function aleatorioEntero(min, max) {
    return parseInt(Math.random() * (max - min) + min + 0.1);
}

function mostrarResultados(valores) {
    for (var i = 0; i < valores.length; i++) {
        document.write("Valor de suma: " + (2 + i) + " Veces aparecido: " + valores[i] + "<br>")
    }
}